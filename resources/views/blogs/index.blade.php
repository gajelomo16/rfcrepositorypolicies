@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>RFC POLICY REPOSITORY</h2>
            </div>
            <div class="pull-right">
                <a  type="button" class="btn btn-success" href="{{ route('blogs.create') }}"> Create new Repository</a>
            </div>
        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif


    <table class="table table-bordered">
        <tr>
            <th>Policy ID</th>
            <th>Document No</th>
            <th>Title</th>
            <th>Description</th>
            <th width="250px">Action</th>
        </tr>
        @foreach ($blogs as $blog)
        <tr>
            <td>{{ $blog->id }}</td>
            <td>{{ $blog->document_no }}</td>
            <td>{{ $blog->title }}</td>
            <td>{{ $blog->title }}</td>
            <td>
                <form action="{{ route('blogs.destroy',$blog->id) }}" method="POST">

                    <a type="button" class="btn btn-info" href="{{ route('blogs.show',$blog->id) }}">Show</a>

                    <a type="button" class="btn btn-primary" href="{{ route('blogs.edit',$blog->id) }}">Edit</a>

                    @csrf
                    @method('DELETE')

                    <button type="submit" class="btn btn-danger" style="border-radius: 0 !important; border-style: groove;">Delete</button>
                    
                </form>
            </td>
        </tr>
        @endforeach
    </table>
    

    {!! $blogs->links() !!}

@endsection
