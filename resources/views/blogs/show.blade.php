@extends('layouts.app')


@section('content')
<div class="row">
    <div class="large-9 large-centered columns">
    <h1 class="entry-title">{!! $blog->title !!}</h1>

    <div class="entry-meta">
    <span class="posted-on">Uploaded on : <a href="#" rel="bookmark"><time class="entry-date published">   {!! ($blog->created_at) !!}</time></a>	</span></div><!-- .entry-meta -->

    </div>

</div>

<div class="row">

    <div class="large-9 large-centered columns">
            <hr>
            <div class="uk-box-shadow-bottom uk-box-shadow-small uk-width-7-15@s" >
                <div class="uk-background-default uk-padding-large">
                    <p style="text-align: center;"><strong>  {!! ($blog->content) !!}</strong></p>
                    <hr>
                            <div class="uk-background-default uk-padding-small uk-width-1-2@s" style="border-style: inset;position:relative;left:49%">
                            <b><i>RESOURCES</i></b>
                             <ul><li class="pdfbutton b20">
                            [PDF] <a href="https://www.officialgazette.gov.ph/downloads/2019/11nov/20191115-EO-95-RRD.pdf">Executive Order No. 95, s. 2019</a>
                           </ul>
                            </div>
                </div>

                </div>
            </div>

    </div>

</div>



@endsection
