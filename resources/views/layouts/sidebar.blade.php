<button class="uk-button uk-button-default"  uk-icon="menu" type="button" style="background-color:#E5DFDF" uk-toggle="target: #offcanvas-nav"></button>



<div id="offcanvas-nav" uk-offcanvas="overlay: false;bg-close:false;">

        <div class="uk-offcanvas-bar">
                <button class="uk-offcanvas-close" type="button" uk-close></button>


            <ul class="uk-nav uk-nav-default">
                    <span class="uk-margin-medium-right" uk-icon="user"> </span>:


                    <hr>
                <li ><a href="{{ route('home')}}"><span class="uk-margin-small-right" uk-icon="icon: home"></span>HOME</a></li>
                <li ><a href="{{ url('/blogs')}}"><span class="uk-margin-small-right" uk-icon="icon: table"></span> Manage Repository Policy</a></li>
                <li ><a href="#"><span class="uk-margin-small-right" uk-icon="icon: users"></span> Users Management</a></li>
                <li ><a href="#"><span class="uk-margin-small-right" uk-icon="icon: settings"></span> System Management</a></li>
                <li ><a href="#"><span class="uk-margin-small-right" uk-icon="icon: tablet-landscape"></span> Users Activity</a></li>
                </li>

                 <div class="watermark">
                        <h5  style="color:red;text-align:center;margin-top:5%">RFC REPOSITORY POLICIES </h5>
                 </div>
            </ul>

        </div>
    </div>
