@extends('layouts.app')



@section('content')


<div class="row" style="margin-bottom:5%" >
        <div class="col-xs-12 col-sm-12 col-md-12" >
            <div class="input-group md-form form-sm form-2 pl-0" >
                    <input class="form-control my-0 py-1 red-border" type="text" placeholder="Search" aria-label="Search">
                    <div class="input-group-append">
                      <span class="input-group-text red lighten-3" id="basic-text1"><i class="fas fa-search text-grey"
                          aria-hidden="true"></i> <a href="#"uk-search-icon></a></span>
                    </div>
            </div>

        </div>
  </div>
  <div class="row" style="margin-bottom:10%;left:40%;position:relative" >
      <h3><span uk-icon="icon:  file-text; ratio: 3" style="color:rgb(18,39,58)"></span>POLICIES REPOSITORY</h3>
  </div>
    @php

$model = DB::table('blogs')->simplePaginate(5);
    @endphp
  @foreach($model  as $blog)


  <div class="row"  >

      <div class="col-xs-12 col-sm-12 col-md-12">
          <div class="form-group">

              <h1><a href="{{ route('blogs.show',$blog->id) }}" style="text-decoration: none;">{!! $blog->title !!}</a></h1>
          </div>
         </div>

      <div class="col-xs-12 col-sm-12 col-md-12">

          <div class="form-group">
              <hr>
              <strong>Description:</strong>
              {!! ($blog->description) !!}

          </div>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-12">
          <div class="form-group">
              <strong>Created At:</strong>

              {!! ($blog->created_at) !!}

          </div>
          <hr>
      </div>
  </div>
  @endforeach
<div style="float:right">
    {{ $model->links() }}
    <br>
</div>
@endsection

